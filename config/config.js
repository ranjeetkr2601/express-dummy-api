const dotenv = require("dotenv");

dotenv.config();

module.exports = {
    DATABASE: process.env.DATABASE_URL,
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
    TOKEN_HEADER_KEY: process.env.TOKEN_HEADER_KEY,
};
