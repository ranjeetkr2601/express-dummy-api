const { DATABASE } = require("../../config/config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(DATABASE);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.tutorials = require("./tutorial.model.js")(sequelize, Sequelize);
db.user = require("./user.model.js")(sequelize, Sequelize);

module.exports = db;